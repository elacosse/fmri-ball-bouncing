# fMRI Ball Bouncing
******************************************
fmri-ball-bouncing : stimulus to run experiment on learning

ToDo

******************************************

# Introduction
============

* Author: Eric Lacosse
* Web site: http://gitlab.com/elacosse/fmri-ball-bouncing
* Version: 0.0.1
* License: The MIT License (MIT) LICENSE


# Project Organization
------------

    ├── LICENSE
    ├── README.md          <- The top-level README for developers using this project.

    │
    ├── environment.yml    <- The conda .yml file for reproducing the analysis environment.
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   │
    │   ├── stimulus	   <- Hardware drivers, psychopy stimuli, associated utilities 
			      to run experiment online, and design information. 


--------

Development status
==================
This is research code. Please report any issues or bugs encountered.

Use Overview
==================
ToDo

Acknowledgements
================
ToDo
