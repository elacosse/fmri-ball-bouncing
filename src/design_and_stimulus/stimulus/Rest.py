#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Ball bouncing experiment - Rest
"""

from __future__ import division

from psychopy import visual, core, event, data, logging
from psychopy.hardware.emulator import launchScan
#from psychopy_helper import launch_window, Params
import BallBouncing
import multiprocessing
import numpy as np
import pandas as pd
import json
import os, sys
import argparse
import time

#infoDlg = gui.DlgFromDict(MR_settings, title='fMRI parameters', order=['TR', 'volumes'])
#if not infoDlg.OK:
#    core.quit()
# settings for launchScan:

def main():
    ############################################################################
    tr = 0.888
    #tr = 2.0
    #tr = 1.692wwwwwwwwwwwwwwwwwwwwww
    MR_settings = {
        'TR': tr,     # duration (sec) per whole-brain volume
        'volumes':  int((5*60)/tr)+1,   # number of whole-brain 3D volumes per scanning run
        'sync': 'w',     # character to use as the sync timing event; assumed to come at start of a volume
        'skip': 5,       # number of volumes lacking a sync pulse at start of scan (for T1 stabilization)
        'sound': False   # in test mode: play a tone as a reminder of scanner noise
        }
    SCREENSIZE = [1920,1080]
    # Generate Stimulus figures and init variables for saving.
    window = visual.Window(SCREENSIZE, monitor="testMonitor", units="pix",
                            screen=1, allowGUI=True, color=[0,0,0],
                            colorSpace='rgb', fullscr=True, waitBlanking=False,
                            blendMode='avg', checkTiming=True)
    window.setRecordFrameIntervals(True)
    window._refreshThreshold=1/60.0 + 0.05
    logging.console.setLevel(logging.WARNING)

    fixation_crosshair = visual.TextStim(window, text='+', color=[-1,-1,-1], pos=(0,0), height=40)
    ############################################################################
    ############################################################################
    globalClock = core.Clock()
    # summary of run timing, for each key press:
    output = u'vol    onset key\n'
    # for i in range(-1 * MR_settings['skip'], 0):
    #     output += u'%d prescan skip (no sync)\n' % i
    output += u"  0    0.000 sync  [Start of scanning run, vol 0]\n"
    duration = MR_settings['volumes']*MR_settings['TR']
    # launch: operator selects Scan or Test (emulate); see API docuwmentation
    vol = launchScan(window, MR_settings, globalClock=globalClock) # presacn


    # Prescan skip, skip begining volumes of scan
    t = globalClock.getTime()
    skip_time = MR_settings['skip']*tr # amount of scans to skip in time
    rest_time = 5*60.0+10.0
    while t < skip_time + rest_time:


        fixation_crosshair.draw()
        window.flip()

        t = globalClock.getTime()
        allKeys = event.getKeys()




        for key in allKeys:
            if key == MR_settings['sync']:
                onset = globalClock.getTime()
                # do your experiment code at this point if you want it sync'd to the TR
                # for demo just display a counter & time, updated at the start of each TR
                #counter.setText(u"%d volumes\n%.3f seconds" % (vol, onset))
                output += u"%3d  %7.3f sync\n" % (vol, onset)
                #counter.draw()
                #window.flip()
                vol += 1
            else:
                # handle keys (many fiber-optic buttons become key-board key-presses)
                output += u"%3d  %7.3f %s\n" % (vol-1, globalClock.getTime(), unicode(key))
                if key == 'escape':
                    core.wait(2.0) # wait to close mr320 before terminating! Also additional time saving
                    output += u'user cancel, '
                    break

    #globalClock = core.Clock()



    t = globalClock.getTime()
    window.flip()
    output += u"End of scan (vol 0..%d = %d of %s). Total duration = %7.3f sec" % (vol - 1, vol, MR_settings['volumes'], t)
    print(output)

    plot_frames = False
    if plot_frames:
        import pylab
        pylab.plot(window.frameIntervals)
        pylab.show()
        df1 = pd.DataFrame(pp_bbsim_delta_t)
        pylab.plot(pp_bbsim_delta_t)
        pylab.show()

    window.close()
    core.quit()

if __name__ == '__main__':
    multiprocessing.freeze_support() # windows thing
    main()
