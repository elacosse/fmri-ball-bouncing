#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Fitts Task experiment

Range of encoder during task approx 166
"""
from psychopy import visual, core, event, data, logging
from psychopy.hardware.emulator import launchScan


#from psychopy_helper import launch_window, Params

import FittsTask
import multiprocessing
import numpy as np
import pandas as pd
import json
import os, sys
import argparse
import time

os.environ['PYGLET_SHADOW_WINDOW']='0'

def create_regs(window, from_center=40.0, width=500, height=7.5,
                    fill_color_up='yellow', fill_color_down='green'):
    """Returns static figures in Fitt's task"""
    pos1 = (0, from_center)
    pos2 = (0, -from_center)


    reg_top = visual.Rect(window, width=width, height=height,
                          pos=pos1, size=1, units=None, fillColor=fill_color_up,
                          fillColorSpace='rgb', interpolate=True)
    top_line = visual.Line(window, start=(-width/2, from_center), end=(width/2, from_center))

    reg_bottom = visual.Rect(window, width=width, height=height,
                          pos=pos2, size=1, units=None, fillColor=fill_color_down,
                          fillColorSpace='rgb', interpolate=True)
    bottom_line = visual.Line(window, start=(-width/2, -from_center), end=(width/2, -from_center))
    return reg_top, reg_bottom, top_line, bottom_line

def create_racket(window, pos, size=8, fill_color='black'):
    racket = visual.Circle(window, edges=32, pos=pos, size=size,
                         units=None, fillColor=fill_color, fillColorSpace='rgb',
                         interpolate=True)

    return racket

def save_data(frame_intervals, sim_time, save_path):
    with open(save_path, 'wb') as f:  # Python 1: open(..., 'w')
        data = {'frame_invervals' : frame_intervals, 'sim_time' : sim_time}
        json.dump(data, f)

#infoDlg = gui.DlgFromDict(MR_settings, title='fMRI parameters', order=['TR', 'volumes'])
#if not infoDlg.OK:
#    core.quit()
# settings for launchScan:

def main():
    ############################################################################
    parser = argparse.ArgumentParser(prog = "bb_exp",
            description = """ Run stimulus """)
    parser.add_argument("-e", "-events", "--events", metavar='N', nargs='+',
                            help = "event design file",
                            type=str)
    args = parser.parse_args()
    event_path = args.events
    ###########################################################################
    df_events = pd.read_csv(event_path[0])
    #save_data_dir = '/home/eric/ownCloud/repos/git-amd/motor_learning_fmri/ball_bouncing/results'
    save_data_dir = 'C:\Users\meduser\Documents\Python Scripts\eric_bb\fmri_ball_bouncing\results'
    #save_data_dir = 'C:/Users/eric/ownCloud/repos/git-amd/fmri_ball_bouncing/results/'
    #save_path = '/home/eric/ownCloud/repos/git-amd/fmri_ball_bouncing/stimulus/test.pickle'
    base = os.path.basename(event_path[0])
    file_id = os.path.splitext(base)[0]
    save_path = os.path.join(save_data_dir, 'ppfitts_' + file_id + '.json')
    ############################################################################
    encoder_range = 166 # approximate range of encoder wrist movement possible
    tr = 1.692
    rest_len = 2.5 # seconds
    trial_len = 17.5
    MR_settings = {
        'TR': 1.692,     # duration (sec) per whole-brain volume
        'volumes': round((df_events['trial_rest_gos'].iloc[-1]+rest_len)/tr),    # number of whole-brain 3D volumes per scanning run
        'sync': 'w',     # character to use as the sync timing event; assumed to come at start of a volume
        'skip': 0,       # number of volumes lacking a sync pulse at start of scan (for T1 stabilization)
        'sound': False   # in test mode: play a tone as a reminder of scanner noise
        }
    SCREENSIZE = [1920,1080]

    ################# Initialize Environment ###################################
    # Create shared variables for simulation
    wait_time = 0.0 # how long should the "do nothing" period be.
    lock = multiprocessing.Lock()
    sim_t = multiprocessing.Value('d', 0.0)
    racket_y = multiprocessing.Value('d', 0)
    run_sim = multiprocessing.Value('b', False)
    run_session = multiprocessing.Value('b', True)
    fitts_goto_var = multiprocessing.Value('b', False)
    fitts_trial_num = multiprocessing.Value('d', -1)

    ######################### WINDOWS ENV -- Scanner computer ##################
    sim = FittsTask.FittsTask('Fitts task sim', sim_t, racket_y=racket_y,
                               fitts_goto_var=fitts_goto_var, fitts_trial_num=fitts_trial_num,
                               run_sim=run_sim, run_session=run_session, save_path=save_path,
                               comport='COM6', multiprocess=True)

    ######################### LINUX ENV -- My computer #########################
    # sim = FittsTask.FittsTask('Fitts task sim', sim_t, racket_y=racket_y,
    #                        fitts_goto_var=fitts_goto_var, fitts_trial_num=fitts_trial_num,
    #                        run_sim=run_sim, run_session=run_session, save_path=save_path,
    #                        comport='/dev/ttyUSB0', multiprocess=True)

    #sim.racket_y.value
    racket_offset = encoder_range/2.0
    init_racket_pos = racket_y.value # 0, make sure centered.

    # Generate Stimulus figures and init variables for saving.
    window = visual.Window(SCREENSIZE, monitor="testMonitor", units="pix",
                            screen=1, allowGUI=True, color=[0,0,0],
                            colorSpace='rgb', fullscr=True, waitBlanking=False,
                            blendMode='avg', checkTiming=True)
    window.setRecordFrameIntervals(True)
    window._refreshThreshold=1/60.0 + 0.05
    logging.console.setLevel(logging.WARNING)

    reg_top, reg_bottom, top_line, bottom_line = create_regs(window, racket_offset-40, width=200, height=13.0)
    racket = create_racket(window, pos=(0,0), size=4)
    ############################################################################
    ############################################################################
    sim.start() # start ball boucning simulation
    globalClock = core.Clock()
    # summary of run timing, for each key press:
    output = u'vol    onset key\n'
    for i in range(-1 * MR_settings['skip'], 0):
        output += u'%d prescan skip (no sync)\n' % i
    output += u"  0    0.000 sync  [Start of scanning run, vol 0]\n"


    #duration = MR_settings['volumes']*MR_settings['TR']

    text = visual.TextStim(window, text='Get Ready!', pos=(0.0, 0.0))
    # launch: operator selects Scan or Test (emulate); see API docuwmentation
    vol = launchScan(window, MR_settings, globalClock=globalClock) # presacn
    sim.run_session.value = True
    sim.run_sim.value = True

    # note: globalClock has been reset to 0.0 by launchScan() + eps ~= 0.01
    trial_len = df_events['trial_rest_gos'].iloc[0]-df_events['trial_gos'].iloc[0]
    rest_len = df_events['trial_gos'].iloc[1]-df_events['trial_rest_gos'].iloc[0]
    duration = df_events['feedback_gos'].iloc[-1]
    print(trial_len, rest_len, duration)
    trial_indx = 0
    sim_time = []
    print("Starting Trial " + str(trial_indx))
    flip_clock = core.Clock()
    t = globalClock.getTime()
    while t < duration:
        allKeys = event.getKeys()
        t = globalClock.getTime()
        #ball.pos = (bb_sim.ball_x.value, bb_sim.ball_y.value)
        racket.pos = (0, sim.racket_y.value - racket_offset)
        #print(racket.pos)
        rand_interval = 2.0*np.random.random() + 0.75
        if flip_clock.getTime() > rand_interval:
            if fitts_goto_var.value == 1:
                fitts_goto_var.value = 0
                reg_top, reg_bottom, top_line, bottom_line = create_regs(window, racket_offset-40,
                                        width=200, height=13.0, fill_color_up='green',
                                        fill_color_down='yellow')

            elif fitts_goto_var.value == 0:
                fitts_goto_var.value = 1
                reg_top, reg_bottom, top_line, bottom_line = create_regs(window, racket_offset-40,
                                        width=200, height=13.0, fill_color_up='yellow',
                                        fill_color_down='green')
            flip_clock = core.Clock()
        ########################################################################
        if t < df_events['trial_rest_gos'].iloc[trial_indx]:
            # Trial running
            #sim_time = sim.t.value
            #sim_time.append(sim_time)
            fitts_trial_num.value = trial_indx
            reg_top.draw()
            reg_bottom.draw()
            top_line.draw()
            bottom_line.draw()
            racket.draw()
            window.flip()
        elif (t > df_events['trial_gos'].iloc[trial_indx]+trial_len) and \
                (t < df_events['trial_rest_gos'].iloc[trial_indx]+rest_len):
            # Rest running
            fitts_trial_num.value = -1
            text.draw()

            #reg_top.draw()
            #reg_bottom.draw()
            #racket.draw()
            # Draw feedback info about performance. Take mode of error.
            #if (t > df_events['feedback_gos'].iloc[trial_indx]) and \
            #   (t < df_events['feedback_gos'].iloc[trial_indx] + 2.0):
            #    e = error.get_obj() # get ctype array
            #    e_mode = int(calc_trial_error(e)) # find trial indicies and return mode
            #    error_text = visual.TextStim(window, 'Error: ' + str(e_mode),
            #        color=(1, 0, 0), colorSpace='rgb', bold=True, height=50)
            #    error_text.draw()
                # Great
                # Good
                # Okay
                # Bad
            window.flip()

        elif t > df_events['trial_rest_gos'].iloc[-1]+rest_len-0.01:
            break # end of run, in case where TR are rounded off extended.
        elif t > df_events['trial_rest_gos'].iloc[trial_indx]+rest_len:
            # Start new trial
            trial_indx += 1
            print("Starting Trial " + str(trial_indx))
            ####################################################################


        for key in allKeys:
            if key == MR_settings['sync']:
                onset = globalClock.getTime()
                # do your experiment code at this point if you want it sync'd to the TR
                # for demo just display a counter & time, updated at the start of each TR
                #counter.setText(u"%d volumes\n%.3f seconds" % (vol, onset))
                output += u"%3d  %7.3f sync\n" % (vol, onset)
                #counter.draw()
                #window.flip()
                vol += 1
            else:
                # handle keys (many fiber-optic buttons become key-board key-presses)
                output += u"%3d  %7.3f %s\n" % (vol-1, globalClock.getTime(), unicode(key))
                if key == 'escape':
                    core.wait(15.0) # wait to close mr320 before terminating! Also additional time saving
                    #sim.terminate()
                    output += u'user cancel, '
                    break
    sim.run_sim.value = False
    sim.run_session.value=  False

    t = globalClock.getTime()
    window.flip()
    output += u"End of scan (vol 0..%d = %d of %s). Total duration = %7.3f sec" % (vol - 1, vol, MR_settings['volumes'], t)
    print(output)
    core.wait(5.0) # wait to close mr320 before terminating! Also additional time saving
    sim.terminate()

    # Save frame intervals and time discrepency between simulation time psychopy time.
    #save_path = os.path.join(save_data_dir, 'psychopy_run_'+str(run_num[0])+'_subjectid_'+str(subject_id[0])+'.json')
    #save_data(window.frameIntervals, time, save_path)

    # plot_frames = False
    # if plot_frames:
    #     import pylab
    #     pylab.plot(window.frameIntervals)
    #     pylab.show()
    #     df1 = pd.DataFrame(pp_bbsim_delta_t)
    #     pylab.plot(pp_bbsim_delta_t)
    #     pylab.show()

    window.close()
    core.quit()

if __name__ == '__main__':
    multiprocessing.freeze_support() # windows thing
    main()
