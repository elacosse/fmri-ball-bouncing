#!/usr/bin/env python
from __future__ import division

import os
import sys
import time
import json
import socket
import warnings
import argparse
import subprocess
from glob import glob
from string import letters
from math import floor
from subprocess import call
from pprint import pformat
import numpy as np
import pandas as pd
from scipy import stats
from numpy.random import RandomState
from psychopy import core, event, visual, sound
from psychopy.monitors import Monitor
from psychopy import logging

class Params(object):
    """Stores all of the parameters needed during the experiment.
    Some parameters are set upon initialization from the file 'params.py',
    others can be set from the command line.
    """
    def __init__(self, mode, p_file='params'):
        """Initializer for the params object.
        Parameters
        ----------
        exp_name: string, name of the dict we want from the param file
        p_file: string, the name of a parameter file
        """
        self.mode = mode
        im = __import__(p_file)
        self.param_module = im
        param_dict = getattr(im, mode)
        for key, val in param_dict.iteritems():
            setattr(self, key, val)

        timestamp = time.localtime()
        self.timestamp = time.asctime(timestamp)
        self.date = time.strftime("%Y-%m-%d", timestamp)
        self.time = time.strftime("%H-%M-%S", timestamp)
        self.git_hash = git_hash()

    def __repr__(self):

        return pformat(self.__dict__)

    def __getitem__(self, key):

        return getattr(self, key)

    def get(self, key, default=None):

        return getattr(self, key, default)

    def set_by_cmdline(self, arglist):
        """Get runtime parameters off the commandline."""
        # Create the parser, set default args
        parser = argparse.ArgumentParser()
        parser.add_argument("-subject", default="test")
        parser.add_argument("-cbid")
        parser.add_argument("-run", type=int, default=1)
        parser.add_argument("-fmri", action="store_true")
        parser.add_argument("-debug", action="store_true")
        parser.add_argument("-nolog", action="store_true")

        # Add additional arguments by experiment
        try:
            func_name = self.exp_name + "_cmdline"
            arg_func = getattr(self.param_module, func_name)
            arg_func(parser)
        except AttributeError:
            pass

        # Parse the arguments
        args = parser.parse_args(arglist)

        # Add command line args to the class dict
        self.__dict__.update(args.__dict__)

        if self.debug:
            self.full_screen = False

        if self.fmri and hasattr(self, "fmri_monitor_name"):
            self.monitor_name = self.fmri_monitor_name

        if self.fmri and hasattr(self, "fmri_screen_number"):
            self.screen_number = self.fmri_screen_number

        if self.fmri and hasattr(self, "fmri_resp_keys"):
            self.resp_keys = self.fmri_resp_keys

        # Build the log file stem with information we now have
        # TODO Perhaps do this in a property in case this isn't called
        kws = dict(subject=self.subject,
                   mode=self.mode,
                   date=self.date,
                   time=self.time,
                   run=self.run)
        self.log_stem = self.log_template.format(**kws)

    def to_text_header(self, fid):
        """Save the parameters to a text file."""
        for key, val in self.__dict__.items():
            if not key.startswith("_"):
                fid.write("# {} : {} \n".format(key, val))

    def to_json(self, fname):
        """Save the parameters to a .json"""
        data = dict([(k, v) for k, v in self.__dict__.items()
                     if not k.startswith("_")])
        del data["param_module"]

        if not fname.endswith(".json"):
            fname += ".json"
        archive_old_version(fname)
        with open(fname, "w") as fid:
            json.dump(data, fid, sort_keys=True, indent=4)


class WindowInfo(object):
    """Container for monitor information."""
    def __init__(self, params):
        """Extracts monitor information from params file and monitors.py."""
        try:
            mod = __import__("monitors")
        except ImportError:
            sys.exit("Could not import monitors.py in this directory.")

        try:
            minfo = getattr(mod, params.monitor_name.replace("-", "_"))
        except IndexError:
            sys.exit("Monitor not found in monitors.py")

        fullscreen = params.get("full_screen", True)
        size = minfo["size"] if fullscreen else (800, 600)

        monitor = Monitor(name=minfo["name"],
                          width=minfo["width"],
                          distance=minfo["distance"])
        monitor.setSizePix(minfo["size"])

        try:
            monitor.setGamma(minfo["gamma"])
        except AttributeError:
            warnings.warn("Could not set monitor gamma table.")

        # Convert from cd/m^2 to psychopy rgb color
        # Note that this ignores the min luminance of the monitor
        window_color = params.mean_luminance / minfo["max_luminance"] * 2 - 1

        # Allow for horizontal mirror view of the whole window
        if params.fmri and params.get("fmri_mirror_horizontal", False):
            viewscale = [-1, 1]
        else:
            viewscale = [1, 1]

        info = dict(units=params.get("monitor_units", "deg"),
                    screen=params.get("screen", 0),
                    fullscr=fullscreen,
                    allowGUI=not fullscreen,
                    color=window_color,
                    size=size,
                    monitor=monitor,
                    viewScale=viewscale)

        if hasattr(params, "blend_mode"):
            info["blendMode"] = params.blend_mode
            if params.blend_mode == "add":
                info["useFBO"] = True

        if "refresh_hz" in minfo:
            self.refresh_hz = minfo["refresh_hz"]

        self.name = params.monitor_name
        self.__dict__.update(info)
self.window_kwargs = info

def launch_window(params, test_refresh=True, test_tol=.5):
    """Open up a presentation window and measure the refresh rate."""
    # Get the monitor parameters
    m = WindowInfo(params)
    stated_refresh_hz = getattr(m, "refresh_hz", None)

    # Initialize the Psychopy window object
    win = visual.Window(**m.window_kwargs)

    # Record the refresh rate we are currently achieving
    if test_refresh or stated_refresh_hz is None:
        win.setRecordFrameIntervals(True)
        logging.console.setLevel(logging.CRITICAL)
        flip_time, _, _ = visual.getMsPerFrame(win)
        observed_refresh_hz = 1000 / flip_time

    # Possibly test the refresh rate against what we expect
    if test_refresh and stated_refresh_hz is not None:
        refresh_error = np.abs(stated_refresh_hz - observed_refresh_hz)
        if refresh_error > test_tol:
            msg = ("Observed refresh rate differs from expected by {:.3f} Hz"
                   .format(refresh_error))
            raise RuntimeError(msg)

    # Set the refresh rate to use in the experiment
    if stated_refresh_hz is None:
        msg = "Monitor configuration does not have refresh rate information"
        warnings.warn(msg)
        win.refresh_hz = observed_refresh_hz
    else:
        win.refresh_hz = stated_refresh_hz

    return win
