#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Ball bouncing experiment
"""

from __future__ import division

from psychopy import visual, core, event, data, logging
from psychopy.hardware.emulator import launchScan
#from psychopy_helper import launch_window, Params
import BallBouncing
import multiprocessing
import numpy as np
import pandas as pd
import json
import os, sys
import argparse
import time

def create_top_bar(window, bar_height, width=1024/2, height=7.5,
                    fill_color='yellow', top_bar_side='left'):
    """Returns static figures in ball bouncing images: top bar."""
    # top bar figure
    if top_bar_side == 'left':
        pos = (-width/2, bar_height)
        top_bar = visual.Rect(window, width=width, height=height,
                              pos=pos, size=1, units=None, fillColor=fill_color,
                              fillColorSpace='rgb', interpolate=True)
    elif top_bar_side == 'right':
        pos = (width/2, bar_height)
        top_bar = visual.Rect(window, width=width, height=height, pos=pos,
                              size=1, units=None, fillColor=fillColor,
                              fillColorSpace='rgb', interpolate=True)
    return top_bar

def create_ball(window, pos, ball_radius=5, fill_color='white'):
    ball = visual.Circle(window, radius=ball_radius, edges=32, pos=pos, size=1,
                         units=None, fillColor=fill_color, fillColorSpace='rgb',
                         interpolate=True)
    return ball

def create_racket(window, pos, width=1024/6, height=7.5, fill_color='yellow'):
    racket = visual.Rect(window, width=width, height=height, pos=pos, size=1,
                         units=None, fillColor=fill_color, fillColorSpace='rgb',
                         interpolate=True)
    return racket

def save_data(frame_intervals, sim_time, save_path):
    with open(save_path, 'wb') as f:  # Python 1: open(..., 'w')
        data = {'frame_invervals' : frame_intervals, 'sim_time' : sim_time}
        json.dump(data, f)

def calc_trial_error(errors, empty_array_element=999):
    """Take list of errors, find indicies in ctype ds, and return mode"""
    try:
        index = errors[0:99].index(empty_array_element)-1 # get index of last element
        e = errors[0:index]
        return np.median(e)
    except:
        return -999
#infoDlg = gui.DlgFromDict(MR_settings, title='fMRI parameters', order=['TR', 'volumes'])
#if not infoDlg.OK:
#    core.quit()
# settings for launchScan:

def main():
    ############################################################################
    parser = argparse.ArgumentParser(prog = "bb_exp",
            description = """ Run stimulus """)
    parser.add_argument("-e", "-events", "--events", metavar='N', nargs='+',
                            help = "event design file",
                            type=str)
    args = parser.parse_args()
    event_path = args.events
    ###########################################################################
    df_events = pd.read_csv(event_path[0])
    #save_data_dir = '/home/eric/ownCloud/repos/git-amd/motor_learning_fmri/ball_bouncing/results'
    save_data_dir = r'C:\Users\meduser\Documents\Python Scripts\eric_bb\eric_bb\results'
    #save_data_dir = 'C:/Users/eric/ownCloud/repos/git-amd/fmri_ball_bouncing/results/'
    #save_path = '/home/eric/ownCloud/repos/git-amd/fmri_ball_bouncing/stimulus/test.pickle'
    base = os.path.basename(event_path[0])
    file_id = os.path.splitext(base)[0]
    save_path = os.path.join(save_data_dir, 'ppbb_' + file_id + '.json')
    ############################################################################
    tr = 0.888
    #tr = 2.0
    #tr = 1.692wwwwwwwwwwwwwwwwwwwwww
    rest_len = 10.0 # seconds
    trial_len = 30.0 # TOTAL LENGTH OF TRIAL INCLUDING REST PERIOD
    MR_settings = {
        'TR': tr,     # duration (sec) per whole-brain volume
        'volumes': round((df_events['trial_rest_gos'].iloc[-1]+rest_len)/tr),    # number of whole-brain 3D volumes per scanning run
        'sync': 'w',     # character to use as the sync timing event; assumed to come at start of a volume
        'skip': 5,       # number of volumes lacking a sync pulse at start of scan (for T1 stabilization)
        'sound': False   # in test mode: play a tone as a reminder of scanner noise
        }
    SCREENSIZE = [1920,1080]
    ################# Initialize Environment ###################################
    # Create shared variables for simulation
    height_res = 1280.0
    ball_radius = 5
    bar_height = height_res/2/3
    bar_thickness = 7.5
    wait_time = 0.0 # how long should the "do nothing" period be.
    ball_x_s = -150.0
    ball_y_s = bar_height+bar_thickness/2+ball_radius
    lock = multiprocessing.Lock()
    sim_t = multiprocessing.Value('d', 0.0)
    error = multiprocessing.Array('d', [999]*100) # Array
    ball_x = multiprocessing.Value('d', ball_x_s)
    ball_y = multiprocessing.Value('d', ball_y_s)
    racket_y = multiprocessing.Value('d', -height_res/3)
    run_sim = multiprocessing.Value('b', False)
    run_session = multiprocessing.Value('b', True)


    ######################### WINDOWS ENV -- Scanner computer ##################
    bb_sim = BallBouncing.BallBouncing('ball bouncing simulation', sim_t, error,
                        bar_height=bar_height, ball_radius=ball_radius,
                        racket_y=racket_y, ball_x=ball_x, ball_y=ball_y,
                        run_sim=run_sim, run_session=run_session,
                        wait_time=wait_time, save_path=save_path,
                        comport='COM6', multiprocess=True)

    ######################### LINUX ENV -- My computer #########################
    # bb_sim = BallBouncing.BallBouncing('ball bouncing simulation', sim_t, error,
    #                     bar_height=bar_height, ball_radius=ball_radius,
    #                     racket_y=racket_y, ball_x=ball_x, ball_y=ball_y,
    #                     run_sim=run_sim, run_session=run_session,
    #                     wait_time=wait_time, save_path=save_path,
    #                     comport='/dev/ttyUSB0', multiprocess=True)

    init_racket_pos = racket_y.value # -height_res/4

    # Generate Stimulus figures and init variables for saving.
    window = visual.Window(SCREENSIZE, monitor="testMonitor", units="pix",
                            screen=1, allowGUI=True, color=[0,0,0],
                            colorSpace='rgb', fullscr=True, waitBlanking=False,
                            blendMode='avg', checkTiming=True)
    window.setRecordFrameIntervals(True)
    window._refreshThreshold=1/60.0 + 0.05
    logging.console.setLevel(logging.WARNING)

    top_bar = create_top_bar(window, bar_height, height=bar_thickness)
    racket = create_racket(window, (bb_sim.racket_x, bb_sim.racket_y.value),
        height=bb_sim.racket_height)
    ball = create_ball(window, (bb_sim.ball_x.value, bb_sim.ball_y.value), ball_radius)

    ############################################################################
    ############################################################################
    bb_sim.start() # start ball boucning simulation
    globalClock = core.Clock()
    # summary of run timing, for each key press:
    output = u'vol    onset key\n'
    # for i in range(-1 * MR_settings['skip'], 0):
    #     output += u'%d prescan skip (no sync)\n' % i
    output += u"  0    0.000 sync  [Start of scanning run, vol 0]\n"
    duration = MR_settings['volumes']*MR_settings['TR']
    # launch: operator selects Scan or Test (emulate); see API docuwmentation
    vol = launchScan(window, MR_settings, globalClock=globalClock) # presacn


    # Prescan skip, skip begining volumes of scan
    t = globalClock.getTime()
    skip_time = MR_settings['skip']*tr # amount of scans to skip in time

    while t < skip_time:
        t = globalClock.getTime()
        allKeys = event.getKeys()
        window.flip()
        for key in allKeys:
            if key == MR_settings['sync']:
                onset = globalClock.getTime()
                # do your experiment code at this point if you want it sync'd to the TR
                # for demo just display a counter & time, updated at the start of each TR
                #counter.setText(u"%d volumes\n%.3f seconds" % (vol, onset))
                output += u"%3d  %7.3f sync\n" % (vol, onset)
                #counter.draw()
                #window.flip()
                vol += 1
            else:
                # handle keys (many fiber-optic buttons become key-board key-presses)
                output += u"%3d  %7.3f %s\n" % (vol-1, globalClock.getTime(), unicode(key))
                if key == 'escape':
                    core.wait(2.0) # wait to close mr320 before terminating! Also additional time saving
                    bb_sim.terminate()
                    output += u'user cancel, '
                    break

    #globalClock = core.Clock()

    bb_sim.run_session.value = True
    bb_sim.run_sim.value = True
    # note: globalClock has been reset to 0.0 by launchScan() + eps ~= 0.01
    trial_indx = 0 # index for trial
    time = []

    print("Starting Trial " + str(trial_indx))
    t = globalClock.getTime()
    while t < duration+skip_time:
        allKeys = event.getKeys()
        t = globalClock.getTime()
        ball.pos = (bb_sim.ball_x.value, bb_sim.ball_y.value)
        racket.pos = (bb_sim.racket_x, bb_sim.racket_y.value)
        ########################################################################
        if t < df_events['trial_rest_gos'].iloc[trial_indx]+skip_time:
            # Trial running
            sim_time = bb_sim.t.value
            time.append(sim_time)
            top_bar.draw()
            racket.draw()
            if bb_sim.ball_y.value > bb_sim.racket_y.value+bar_thickness: # don't display ball if below racket.
                ball.draw()
            else:
                ball.pos = (bb_sim.ball_x.value, bb_sim.ball_y.value+bar_thickness)
                ball.draw()
            window.flip()
        elif t < df_events['trial_gos'].iloc[trial_indx]+trial_len+skip_time:
            # Rest running
            ## Reset ball to starting height and end trial
            bb_sim.run_sim.value = False # exit simulation trial
            top_bar.draw()
            racket.draw()
            # Draw feedback info about performance. Take mode of error.
            if (t > df_events['feedback_gos'].iloc[trial_indx] + skip_time) and \
               (t < df_events['feedback_gos'].iloc[trial_indx] + 2.0  +skip_time):
                e = error.get_obj() # get ctype array
                e_mode = int(calc_trial_error(e)) # find trial indicies and return mode
                error_text = visual.TextStim(window, 'Error: ' + str(e_mode),
                    color=(1, 0, 0), colorSpace='rgb', bold=True, height=50)
                error_text.draw()
                # Great
                # Good
                # Okay
                # Bad
            window.flip()
        elif t > df_events['trial_rest_gos'].iloc[-1]+rest_len+skip_time:
            break # end of run, in case where TR are rounded off extended.
        else:
            #print(e_mode)
            # Start new trial
            #print(bb_trial_go[trial_indx], bb_trial_rest[trial_indx], t)
            bb_sim.run_sim.value = True
            trial_indx += 1
            print("Starting Trial " + str(trial_indx))
            ####################################################################
        for key in allKeys:
            if key == MR_settings['sync']:
                onset = globalClock.getTime()
                # do your experiment code at this point if you want it sync'd to the TR
                # for demo just display a counter & time, updated at the start of each TR
                #counter.setText(u"%d volumes\n%.3f seconds" % (vol, onset))
                output += u"%3d  %7.3f sync\n" % (vol, onset)
                #counter.draw()
                #window.flip()
                vol += 1
            else:
                # handle keys (many fiber-optic buttons become key-board key-presses)
                output += u"%3d  %7.3f %s\n" % (vol-1, globalClock.getTime(), unicode(key))
                if key == 'escape':
                    core.wait(10.0) # wait to close mr320 before terminating! Also additional time saving
                    bb_sim.terminate()
                    output += u'user cancel, '
                    break

    bb_sim.run_sim.value = False
    bb_sim.run_session.value=  False

    t = globalClock.getTime()
    window.flip()
    output += u"End of scan (vol 0..%d = %d of %s). Total duration = %7.3f sec" % (vol - 1, vol, MR_settings['volumes'], t)
    print(output)
    core.wait(20.0) # wait to close mr320 before terminating! Also additional time saving
    bb_sim.terminate()

    # Save frame intervals and time discrepency between simulation time psychopy time.
    save_path = os.path.join(save_data_dir, 'psychopy_'+ file_id +'.json')
    save_data(window.frameIntervals, time, save_path)

    plot_frames = False
    if plot_frames:
        import pylab
        pylab.plot(window.frameIntervals)
        pylab.show()
        df1 = pd.DataFrame(pp_bbsim_delta_t)
        pylab.plot(pp_bbsim_delta_t)
        pylab.show()

    window.close()
    core.quit()

if __name__ == '__main__':
    multiprocessing.freeze_support() # windows thing
    main()
