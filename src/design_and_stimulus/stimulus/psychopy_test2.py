from psychopy import visual, core  # import some libraries from PsychoPy


from psychopy.hardware.emulator import launchScan

SCREENSIZE = [1920,1080]

window = visual.Window(SCREENSIZE, monitor="testMonitor", units="pix",
                        screen=1, allowGUI=True, color=[0,0,0],
                        colorSpace='rgb', fullscr=True, waitBlanking=False,
                        blendMode='avg', checkTiming=True)
window.setRecordFrameIntervals(True)
window._refreshThreshold=1/60.0 + 0.05
#logging.console.setLevel(logging.WARNING)

tr = 1.628
rest_len = 4.0 # seconds
trial_len = 24.0
MR_settings = {
    'TR': 1.628,     # duration (sec) per whole-brain volume
    'volumes': 10,    # number of whole-brain 3D volumes per scanning run
    'sync': 'w',     # character to use as the sync timing event; assumed to come at start of a volume
    'skip': 5,       # number of volumes lacking a sync pulse at start of scan (for T1 stabilization)
    'sound': False   # in test mode: play a tone as a reminder of scanner noise
    }

vol = launchScan(window, MR_settings) # presacn
# #create a window
# mywin = visual.Window([800,600], monitor="testMonitor", units="deg")
#
# #create some stimuli
# grating = visual.GratingStim(win=mywin, mask="circle", size=3, pos=[-4,0], sf=3)
# fixation = visual.GratingStim(win=mywin, size=0.5, pos=[0,0], sf=0, rgb=-1)
#
# #draw the stimuli and update the window
# grating.draw()
# fixation.draw()
# mywin.update()
#
# #pause, so you get a chance to see it!
# core.wait(5.0)
