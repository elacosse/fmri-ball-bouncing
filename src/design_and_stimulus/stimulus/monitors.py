"""Hold information about different monitors."""
from textwrap import dedent

office_1st = dict(monitor_name='office_1st',
                  width=103.8,
                  distance=50,
                  size=[2560, 1600],
                  notes=dedent('30" LCD display - not yet calibrated'))

office_2nd = dict(monitor_name='office_2nd',
                  width=103.8,
                  distance=50,
                  size=[1024, 1280],
                  notes=dedent('20" LCD display - not yet calibrated'))
