import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import traceback
import sys
import ipdb

from pykalman import KalmanFilter


def load_data(save_path, bar_height=222.083333):
    import json
    with open(save_path, 'rb') as f:  # Python 2: open(..., 'r')
        data = json.load(f)
    trial_ball_racket_ty = data['trial_ball_racket_pos_data']
    trial_apex_heights = data['trial_apex_heights']
    trial_impact_times = data['trial_impact_times']

    run_error = []
    for (ball_racket_ty, apex_heights, impact_times) in zip(
            trial_ball_racket_ty, trial_apex_heights, trial_impact_times):
        df_ball_racket_ty = pd.DataFrame(ball_racket_ty,
                                         columns=['time', 'racket_y', 'ball_y'])
        df_apex_heights = pd.DataFrame(apex_heights, columns=['apex_heights'])
        df_apex_heights = df_apex_heights.iloc[0:22]
        df_impact_times = pd.DataFrame(impact_times, columns=['impact_times'])
        df_ball_racket_ty.index = df_ball_racket_ty['time']
        for x in np.abs(df_apex_heights - bar_height).as_matrix()[0]:
            run_error.append(x)
        error, df_ball_racket_ty = pd.DataFrame(run_error, columns=[
            'error']), df_ball_racket_ty

        time = df_ball_racket_ty['time'].as_matrix()
        measurements = df_ball_racket_ty['racket_y'].as_matrix()

        return time, measurements


def test():
    # parameters ---------------------------------------------------------------
    dt = 0.001
    q = 10.0 ** 6  # this parameter determines how much noise is put on the
    # jerk, this is the main tuning parameter

    random_state = np.random.RandomState(0)

    # state transition model ---------------------------------------------------
    transition_matrix = [[1, dt, dt ** 2 / 2],
                         [0, 1, dt],
                         [0, 0, 1]]
    transition_offset = [0., 0., 0.]
    transition_covariance = np.array([[dt ** 5 / 20, dt ** 4 / 8, dt ** 3 / 6],
                                      [dt ** 4 / 8, dt ** 3 / 3, dt ** 2 / 2],
                                      [dt ** 3 / 6, dt ** 2 / 2, dt]]) * q

    # observation model --------------------------------------------------------
    observation_matrix = [[1, 0, 0]]
    observation_offset = [0]
    observation_covariance = [[1]]

    initial_state_mean = [0, 0, 0]
    initial_state_covariance = np.identity(3) * 1000000

    # create the filter --------------------------------------------------------
    kf = KalmanFilter(
        transition_matrix, observation_matrix, transition_covariance,
        observation_covariance, transition_offset, observation_offset,
        initial_state_mean, initial_state_covariance,
        random_state=random_state
    )

    # filter -------------------------------------------------------------------
    time, observations = load_data('test2_ppclock.json')
    filtered_state_estimates = kf.filter(observations)[0]

    # draw estimates
    plt.figure()
    filtered_position_plot = plt.plot(filtered_state_estimates[:, 0], color='r')
    filtered_velocity_plot = plt.plot(filtered_state_estimates[:, 1], color='k')
    observation_plot = plt.plot(observations, color='g')

    plt.legend((filtered_position_plot[0],
                observation_plot[0],
                filtered_velocity_plot[0]),
               ('filt pos', 'obs', 'filt vel'), loc='lower right')
    plt.show()


if __name__ == '__main__':
    try:
        test()

    except:
        traceback.print_exc(sys.stdout)
        _, _, tb = sys.exc_info()
        ipdb.post_mortem(tb)
