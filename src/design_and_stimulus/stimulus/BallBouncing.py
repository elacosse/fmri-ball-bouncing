import multiprocessing
import numpy as np
import sys, os
import time
import json
from psychopy import core
import timeit, time
import MR320
import kalman

class BallBouncing(multiprocessing.Process):
    """ Run simulation in seperate process and feed results out to another"""

    def __init__(self, name, t, error, bar_height, ball_radius, racket_y,
                 ball_x, ball_y, run_sim=None, run_session=None, wait_time=1.0,
                 clock=None, save_path='', comport='/dev/ttyUSB0',
                 multiprocess=True):
        if multiprocess:
            multiprocessing.Process.__init__(self)
            self.name = name
            sys.stdout.write('[%s] created \n' % (self.name))

        self.save_path=save_path
        self.clock = clock
        # simulation parameters
        self.pixels_per_meter = 3368.4210526315787
        self.g = self.pixels_per_meter*9.8/5
        self.alpha = 0.8
        #self.alpha = 0.99999
        self.ball_vx = 100
        self.ball_state = 'do_nothing' # default state of simulation
        self.trial_running = True
        self.target_hit = False

        self.wait_time = wait_time
        self.save_path = save_path

        self.bar_height = bar_height
        self.fall_height = bar_height #+ball_radius*2 # initial fall height
        self.init_ball_x = ball_x.value # set ball to initial x on initialization.
        self.racket_height = 7.5
        self.racket_x = 0.0 # racket centered at 0
        ### Multiprocessing Synchronized Variables #############################
        self.t = t # multiprocessing.Value
        self.error = error # multiprocessing.Array
        self.racket_y = racket_y
        self.ball_x = ball_x
        self.ball_y = ball_y
        self.run_sim = run_sim
        self.run_session = run_session
        ########################################################################
        # default states
        self.ball_x_s = self.ball_x.value
        self.ball_y_s = self.ball_y.value

        # simulation variables recorded
        self.ball_racket_ty = []
        self.impact_times = []
        self.apex_heights = []
        self.mr320_pos = []
        self.trial_impact_times = []
        self.trial_apex_heights = []
        self.trial_ball_racket_pos_data = []
        self.trial_mr320_pos = []

        # Initialize Kalman filter
        self.kf = kalman.kalman(dt=0.001)
        self.kf_mean = np.array([0, 0, 0]) # init mean, pos, vel, accel
        self.kf_cov = np.identity(3) * 1000000 # init cov
        ########################################################################
        self.init_racket_pos = self.racket_y.value
        self.mr320 = None
        self.comport = comport

    def save_data(self):
        """Saves data results from simulation into specified save_path."""
        # Saving the objects:
        with open(self.save_path, 'wb') as f:  # Python 1: open(..., 'w')
            #print(len(self.trial_ball_racket_pos_data))
            #print(sys.getsizeof(self.trial_ball_racket_pos_data[0]))
            #print(sys.getsizeof(self.trial_ball_racket_pos_data))
            data = {'trial_ball_racket_pos_data' : self.trial_ball_racket_pos_data,
                    'trial_impact_times' : self.trial_impact_times,
                    'trial_apex_heights' : self.trial_apex_heights,
                    'trial_mr320_position' : self.trial_mr320_pos}
            json.dump(data, f)
        print("Saving finished!")



    def initialize_MR320(self):
        try:
            self.mr320 = MR320.MR320(self.comport)
            self.mr320.print_config()
            self.mr320.write_and_set_to_cont_talker(1) # receive at 1ms
            self.mr320.serial.close()
        except:
            print("WARNING: Couldn't set MR320 as continous talker.")
        #Now 250ms to catch bytes! Open serial interface at new baudrate.
        try:
            self.mr320 = MR320.serialMR320(self.comport)
        except:
            print("Couldn't set MR320 serial connection.")


    def get_ball_pos(self):
        """Return ball position as tuple"""
        return (self.ball_x.value, self.ball_y.value)

    def get_racket_pos(self):
        """Return racket position as tuple"""
        return (self.racket_x, self.racket_y.value)

    def update_kalman(self, new_measurement):
        """Updates Kalman filter"""
        self.kf_mean, self.kf_cov = self.kf.filter_update(self.kf_mean, self.kf_cov,
                                        new_measurement)
    # def estimate_racket_vel(self):
    #     """Estimates racket velocity"""
    #     # # Naive way--Euler approx.
    #     # racket_impact_vel = (self.racket_y.value-\
    #     #                             self.ball_racket_ty[-50][1])/\
    #     #                             (self.t.value-\
    #     #                             self.ball_racket_ty[-50][0])
    #
    #     # Kalman filter estimation
    #     return racket_impact_vel

    def run_simulation(self):
        #try:
        if True:
            # Set MR320 as continuous talker
            self.initialize_MR320() # needs to continually take data from device
            while True:
                if self.run_session.value == False:
                    self.mr320.stop_talker()
                    break
                ################################################################
                # Get racket position
                mr320_pos = self.mr320.get_position()
                if mr320_pos is not None:
                #    self.racket_y.value = mr320_pos[0] + self.init_racket_pos \
                #            + self.racket_height
                    self.update_kalman(mr320_pos[0])
                    self.racket_y.value = self.kf_mean[0] + self.init_racket_pos \
                        + self.racket_height
                self.ball_x.value = self.ball_x_s
                self.ball_y.value = self.ball_y_s
                self.ball_state = 'do_nothing'
                ################################################################
                if self.run_sim.value == True:
                    t_start = timeit.default_timer()
                    self.ball_racket_ty = []
                    self.impact_times = []
                    self.apex_heights = []
                    self.mr320_pos = []
                    apex_count_indx = 0
                    if len(self.ball_racket_ty) != []:
                        self.trial_ball_racket_pos_data.append(self.ball_racket_ty)
                        self.trial_impact_times.append(self.impact_times)
                        self.trial_apex_heights.append(self.apex_heights)
                        self.trial_mr320_pos.append(self.mr320_pos)
                while self.run_sim.value:
                    mr320_pos = self.mr320.get_position()
                    t = timeit.default_timer()-t_start
                    self.t.value = t
                    if mr320_pos is not None:
                        self.update_kalman(mr320_pos[0])
                        self.racket_y.value = self.kf_mean[0] + self.init_racket_pos \
                            + self.racket_height
                        #self.racket_y.value = mr320_pos[0] + self.init_racket_pos \
                        #        + self.racket_height
                    if self.ball_state == 'fall':
                        # ball flight down
                        ball_d = -(0.5)*self.g*(t-time_fall)**2
                        self.ball_y.value = self.fall_height + ball_d
                        if (self.ball_y.value-self.racket_y.value) <= 0:
                            # ball-racket impact
                            racket_impact_vel = self.kf_mean[1]
                            #racket_impact_vel = self.estimate_racket_vel()
                            # set ball to racket position.
                            self.ball_y.value = self.racket_y.value
                            racket_impact_y = self.ball_y.value
                            time_impact = t
                            self.impact_times.append(time_impact)
                            ball_vel = -self.g*(t-time_fall)
                            release_vel = -self.alpha*(ball_vel-racket_impact_vel) \
                                            + racket_impact_vel
                            self.impact_times.append(time_impact)
                            self.ball_state = 'bounce'

                    elif self.ball_state == 'bounce':
                        # ball flight up
                        ball_d = -(0.5)*self.g*(t-time_impact)**2
                        self.ball_y.value = racket_impact_y + (release_vel*(t-time_impact)\
                                            +ball_d)
                        # ball reaches apex
                        if (self.ball_y.value-self.ball_racket_ty[-1][2]) <= 0.0:
                            self.fall_height = self.ball_y.value # - 2.0 due to sampling issue
                            try:
                                self.error.get_obj()[apex_count_indx] = abs(self.ball_y.value-self.bar_height) # - 11 due to sampling issue
                                print(self.fall_height, abs(self.ball_y.value-self.bar_height), racket_impact_vel)
                                apex_count_indx += 1
                            except IndexError:
                                pass
                            self.apex_heights.append(self.fall_height)
                            time_fall = t
                            self.ball_state = 'fall'

                    elif self.ball_state == 'roll':
                        self.error.get_obj()[:] = [999]*100 # reset error

                        self.ball_x.value = self.init_ball_x+self.ball_vx*t
                        if self.ball_x.value >= 0:
                            self.ball_x.value = 0.0
                            time_fall = t
                            self.fall_height = self.ball_y.value
                            self.ball_state = 'fall'

                    elif self.ball_state == 'do_nothing':
                        if t >= self.wait_time:
                            t_start += t
                            self.ball_state = 'roll'
                    ################################################################
                    # Append positions and simulation result in trial
                    self.ball_racket_ty.append((t, self.racket_y.value, self.ball_y.value))
                    self.mr320_pos.append((t, mr320_pos[0]))

        #except :
        #    sys.stdout.write('[%s] could not run simulation \n' % (self.name))
        print("Successfully exited the simulation session. Saving. Wait!")
        self.save_data()

    def run_simulation_test(self):
        #try:
        if True:
            # Set MR320 as continuous talker
            #self.initialize_MR320() # needs to continually take data from device
            while True:
                if self.run_session.value == False:
                    #self.mr320.stop_talker()
                    break
                ################################################################
                # Get racket position
                mr320_pos = (0,0) #self.mr320.get_position()
                if mr320_pos is not None:
                #    self.racket_y.value = mr320_pos[0] + self.init_racket_pos \
                #            + self.racket_height
                    self.update_kalman(mr320_pos[0])
                    self.racket_y.value = self.kf_mean[0] + self.init_racket_pos \
                        + self.racket_height
                self.ball_x.value = self.ball_x_s
                self.ball_y.value = self.ball_y_s
                self.ball_state = 'do_nothing'
                ################################################################
                if self.run_sim.value == True:
                    t_start = timeit.default_timer()
                    self.ball_racket_ty = []
                    self.impact_times = []
                    self.apex_heights = []
                    self.mr320_pos = []
                    apex_count_indx = 0
                    if len(self.ball_racket_ty) != []:
                        self.trial_ball_racket_pos_data.append(self.ball_racket_ty)
                        self.trial_impact_times.append(self.impact_times)
                        self.trial_apex_heights.append(self.apex_heights)
                        self.trial_mr320_pos.append(self.mr320_pos)
                while self.run_sim.value:
                    mr320_pos = (0,0) #self.mr320.get_position()
                    t = timeit.default_timer()-t_start
                    self.t.value = t
                    if mr320_pos is not None:
                        self.update_kalman(mr320_pos[0])
                        self.racket_y.value = self.kf_mean[0] + self.init_racket_pos \
                            + self.racket_height
                        #self.racket_y.value = mr320_pos[0] + self.init_racket_pos \
                        #        + self.racket_height
                    if self.ball_state == 'fall':
                        # ball flight down
                        ball_d = -(0.5)*self.g*(t-time_fall)**2
                        self.ball_y.value = self.fall_height + ball_d
                        if (self.ball_y.value-self.racket_y.value) <= 0:
                            # ball-racket impact
                            racket_impact_vel = self.kf_mean[1]
                            #racket_impact_vel = self.estimate_racket_vel()
                            # set ball to racket position.
                            self.ball_y.value = self.racket_y.value
                            racket_impact_y = self.ball_y.value
                            time_impact = t
                            self.impact_times.append(time_impact)
                            ball_vel = -self.g*(t-time_fall)
                            release_vel = -self.alpha*(ball_vel-racket_impact_vel) \
                                            + racket_impact_vel
                            self.impact_times.append(time_impact)
                            self.ball_state = 'bounce'

                    elif self.ball_state == 'bounce':
                        # ball flight up
                        ball_d = -(0.5)*self.g*(t-time_impact)**2
                        self.ball_y.value = racket_impact_y + (release_vel*(t-time_impact)\
                                            +ball_d)
                        # ball reaches apex
                        if (self.ball_y.value-self.ball_racket_ty[-1][2]) <= 0.0:
                            self.fall_height = self.ball_y.value # - 2.0 due to sampling issue
                            try:
                                self.error.get_obj()[apex_count_indx] = abs(self.ball_y.value-self.bar_height) # - 11 due to sampling issue
                                print b(self.fall_height, abs(self.ball_y.value-self.bar_height), racket_impact_vel)
                                apex_count_indx += 1
                            except IndexError:
                                pass
                            self.apex_heights.append(self.fall_height)
                            time_fall = t
                            self.ball_state = 'fall'

                    elif self.ball_state == 'roll':
                        self.ball_x.value = self.init_ball_x+self.ball_vx*t
                        if self.ball_x.value >= 0:
                            self.ball_x.value = 0.0
                            time_fall = t
                            self.fall_height = self.ball_y.value
                            self.ball_state = 'fall'

                    elif self.ball_state == 'do_nothing':
                        if t >= self.wait_time:
                            t_start += t
                            self.ball_state = 'roll'
                    ################################################################
                    # Append positions and simulation result in trial
                    self.ball_racket_ty.append((t, self.racket_y.value, self.ball_y.value))
                    self.mr320_pos.append((t, mr320_pos[0]))

        #except :
        #    sys.stdout.write('[%s] could not run simulation \n' % (self.name))
        print("Successfully exited the simulation session. Saving. Wait!")
        self.save_data()

    def run(self):
        sys.stdout.write('[%s] started ...  process id: %s\n'
                         % (self.name, os.getpid()))
        #self.run_simulation_test()
        self.run_simulation()
        sys.stdout.write('[%s] finished.\n' % (self.name))

if (__name__ == "__main__"):
    height_res = 1280.0
    ball_radius = 5
    bar_height = height_res/2/3
    wait_time = 0.0 # how long should the "do nothing" period be.
    ball_x_s = -100.0
    ball_y_s = bar_height+ball_radius*2
    lock = multiprocessing.Lock()
    sim_t = multiprocessing.Value('d', 0.0)
    error = multiprocessing.Array('d', [999]*100) # Array
    ball_x = multiprocessing.Value('d', -100)
    ball_y = multiprocessing.Value('d', bar_height+ball_radius*2)
    racket_y = multiprocessing.Value('d', -height_res/4)
    run_sim = multiprocessing.Value('b', False)
    run_session = multiprocessing.Value('b', True)
    save_data_dir = 'C:/Users/eric/ownCloud/repos/git-amd/fmri_ball_bouncing/results/'
    save_path = os.path.join(save_data_dir, 'sandy.pickle')
    bb_sim = BallBouncing.BallBouncing('ball bouncing simulation', sim_t, error,
                        bar_height=bar_height, ball_radius=ball_radius,
                        racket_y=racket_y, ball_x=ball_x, ball_y=ball_y,
                        run_sim=run_sim, run_session=run_session,
                        wait_time=wait_time, save_path=save_path,
                        multiprocess=True)
    bb_sim.run_simulation_test()
