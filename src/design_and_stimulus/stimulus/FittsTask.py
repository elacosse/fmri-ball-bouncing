import multiprocessing
import numpy as np
import sys, os
import time
import json
from psychopy import core
import timeit, time
import MR320
import kalman

class FittsTask(multiprocessing.Process):
    """ Run simulation in seperate process and feed results out to another"""

    def __init__(self, name, t, racket_y, fitts_goto_var, fitts_trial_num,
                 run_sim=None, run_session=None, clock=None, save_path='',
                 comport='/dev/ttyUSB0', multiprocess=True):
        if multiprocess:
            multiprocessing.Process.__init__(self)
            self.name = name
            sys.stdout.write('[%s] created \n' % (self.name))

        self.save_path=save_path
        self.clock = clock
        # simulation parameters

        #self.wait_time = wait_time
        self.save_path = save_path

        self.racket_height = 0.0
        self.racket_x = 0.0 # racket centered at 0
        ### Multiprocessing Synchronized Variables #############################
        self.t = t # multiprocessing.Value
        #self.error = error # multiprocessing.Array
        self.racket_y = racket_y
        self.run_sim = run_sim
        self.run_session = run_session
        self.fitts_goto_var = fitts_goto_var
        self.fitts_trial_num = fitts_trial_num
        ########################################################################

        # simulation variables recorded
        self.mr320_pos = []
        self.trail_time = []
        self.trial_mr320_pos = []


        # Initialize Kalman filter
        self.kf = kalman.kalman(dt=0.001)
        self.kf_mean = np.array([0, 0, 0]) # init mean, pos, vel, accel
        self.kf_cov = np.identity(3) * 1000000 # init cov
        ########################################################################
        self.init_racket_pos = self.racket_y.value
        self.mr320 = None
        self.comport = comport

    def save_data(self):
        """Saves data results from simulation into specified save_path."""
        # Saving the objects:
        with open(self.save_path, 'wb') as f:  # Python 1: open(..., 'w')
            #print(len(self.trial_ball_racket_pos_data))
            #print(sys.getsizeof(self.trial_ball_racket_pos_data[0]))
            #print(sys.getsizeof(self.trial_ball_racket_pos_data))
            data = {'trial_mr320_position' : self.trial_mr320_pos}
            json.dump(data, f)
        print("Saving finished!")



    def initialize_MR320(self):
        try:
            self.mr320 = MR320.MR320(self.comport)
            self.mr320.print_config()
            self.mr320.write_and_set_to_cont_talker(1) # receive at 1ms
            self.mr320.serial.close()
        except:
            print("WARNING: Couldn't set MR320 as continous talker.")
        #Now 250ms to catch bytes! Open serial interface at new baudrate.
        try:
            self.mr320 = MR320.serialMR320(self.comport)
        except:
            print("Couldn't set MR320 serial connection.")

    def get_racket_pos(self):
        """Return racket position as tuple"""
        return (self.racket_x, self.racket_y.value)

    def update_kalman(self, new_measurement):
        """Updates Kalman filter"""
        self.kf_mean, self.kf_cov = self.kf.filter_update(self.kf_mean, self.kf_cov,
                                        new_measurement)

    def run_simulation(self):
        #try:
        if True:
            # Set MR320 as continuous talker
            self.initialize_MR320() # needs to continually take data from device
            #mr320_pos = self.mr320.get_position()
            #print(mr320_pos)
            #self.racket_height = mr320_pos[0] # update initial racket height

            while True:

                if self.run_session.value == False:
                    self.mr320.stop_talker()
                    break
                ################################################################
                # Get racket position
                mr320_pos = self.mr320.get_position()
                if mr320_pos is not None:
                #    self.racket_y.value = mr320_pos[0] + self.init_racket_pos \
                #            + self.racket_height
                    self.update_kalman(mr320_pos[0])
                    self.racket_y.value = self.kf_mean[0] + self.init_racket_pos \
                        + self.racket_height
                ################################################################
                if self.run_sim.value == True:
                    # FIRST THING
                    t_start = timeit.default_timer() # start of trial, t
                    self.mr320_pos = [] # set to empty
                    if len(self.mr320_pos) != []:
                        self.trial_mr320_pos.append(self.mr320_pos)
                while self.run_sim.value:
                    mr320_pos = self.mr320.get_position()
                    t = timeit.default_timer()-t_start
                    self.t.value = t
                    if mr320_pos is not None:
                        self.update_kalman(mr320_pos[0])
                        self.racket_y.value = self.kf_mean[0] + self.init_racket_pos \
                            + self.racket_height
                    ################################################################
                    # Append positions and simulation result in trial
                    self.mr320_pos.append((t, mr320_pos[0], self.fitts_goto_var.value, self.fitts_trial_num.value))

        #except :
        #    sys.stdout.write('[%s] could not run simulation \n' % (self.name))
        print("Successfully exited the simulation session. Saving. Wait!")
        self.save_data()

    def run(self):
        sys.stdout.write('[%s] started ...  process id: %s\n'
                         % (self.name, os.getpid()))
        #self.run_simulation_test()
        self.run_simulation()
        sys.stdout.write('[%s] finished.\n' % (self.name))

# if (__name__ == "__main__"):
#     height_res = 1280.0
#     ball_radius = 5
#     bar_height = height_res/2/3
#     wait_time = 0.0 # how long should the "do nothing" period be.
#     ball_x_s = -100.0
#     ball_y_s = bar_height+ball_radius*2
#     lock = multiprocessing.Lock()
#     sim_t = multiprocessing.Value('d', 0.0)
#     error = multiprocessing.Array('d', [999]*100) # Array
#     ball_x = multiprocessing.Value('d', -100)
#     ball_y = multiprocessing.Value('d', bar_height+ball_radius*2)
#     racket_y = multiprocessing.Value('d', -height_res/4)
#     run_sim = multiprocessing.Value('b', False)
#     run_session = multiprocessing.Value('b', True)
#     save_data_dir = 'C:/Users/eric/ownCloud/repos/git-amd/fmri_ball_bouncing/results/'
#     save_path = os.path.join(save_data_dir, 'sandy.pickle')
#     bb_sim = BallBouncing.BallBouncing('ball bouncing simulation', sim_t, error,
#                         bar_height=bar_height, ball_radius=ball_radius,
#                         racket_y=racket_y, ball_x=ball_x, ball_y=ball_y,
#                         run_sim=run_sim, run_session=run_session,
#                         wait_time=wait_time, save_path=save_path,
#                         multiprocess=True)
#     bb_sim.run_simulation_test()
