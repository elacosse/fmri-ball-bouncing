#!/usr/bin/env python
import minimalmodbus
import serial
import binascii
import array
import sys
import math
import ctypes
import struct
import time
import json
## ---------- structure and union for juggling registers vs. long data type -----
##  uses ctypes4

from ctypes import *

class regs(ctypes.Structure):
      _fields_ = [("r0", c_uint16),
                  ("r1", c_uint16)]

class values(ctypes.Union):
    _fields_ = [("regs", regs),
                ("val", c_int32)]

class MR320( minimalmodbus.Instrument ):
    """Instrument class for MR320 fiber optic rotary encoder controller.

    Args:
        * portname (str): port name
    """

    SLAVE_ADDRESS = 234 # default; this is the MR320 controller device address
    record_fetch = False

    def __init__(self, portname, baudrate=9600):
        registers = values()  ## instantiate helper variable
        try:
            minimalmodbus.Instrument.__init__(self, portname, self.SLAVE_ADDRESS)
            self.serial.baudrate = baudrate
            self.mode = minimalmodbus.MODE_RTU  # rtu or ascii mode
            self.serial.timeout  = 0.1          # seconds
            print('--- MR320 Modbus initialized ------------------------------')
        except:
            print('Port {0} connection failed'.format(portname))
            sys.exit(0)


    def print_config(self):
        print('--- serial configuration --------------------------------------')
        print('---------------------------------------------------------------')
        model = self.get_string_at(int('0x400',16),4)
        version = self.get_string_at(int('0x404',16),4)
        serialnumber = self.read_long(int('0x408',16), 3, False) # Registernumber, Function 3, Signed
        print('  Model: {0} '.format(model))
        print('Version: {0} '.format(version))
        print('    S/N: {0} '.format(serialnumber))
        print('---------------------------------------------------------------')

    def get_string_at(self, addr, length):
        ## ----------------  Function getstringat(address) -------------------
        ##
        ## The model name is stored in 4 registers with two characters in each register
        ## Modbus address is 0x400 length is 4 registers
        ## Use the instrument.read_registers(address, number of registers, function code 3)
        ## Once read the characters need to be extrated.
        ## there maybe a more effcient way in Python to do this
        ##
        ## 1st Prepare an integer array to red the 4 registers
        ## 2nd Prepare a byte array of 8 bytes so we can extract the model name
        ##
        result = 'model'
        modelregs =array.array('I',(0,)*4)  ## regiters as read from device
        mdl = bytearray()  ## byte array
        ## Read the 4 registers containing the string
        modelregs = self.read_registers(addr,length,3)   # address, number registers, func 3
        ## Now extract the character values
        for c in modelregs:
            mdl.extend(divmod(c,256))
        return mdl.rstrip(chr(0))

    def get_status(self):
        ## Read Status Register from Device  Address is 0
        status = self.read_register(0, 0) # Registernumber, number of decimals
        print('Status: {0}'.format(status))
        return status

    def get_position(self):
        ## Read Position Information from Device using Long command Address is 1 and 2 words
        pos = self.read_long(1, 3, True)
        return pos

    def write_voltage_scale(self, voltage_scale = 1528):
        self.write_long(int('0x201',16),voltage_scale)
        v = self.read_long(int('0x201',16),3,False)   # address, func 3, unsigned
        print('Scale set to {0} '.format(v))

    def write_turn_direction(self, turn_direction=0):
        ## Write the Turn Direction value at address 0x20B
        #turn_direction = 0  ## CW  (CCW=1)
        self.write_register(int('0x20B',16), turn_direction, 0, 16,False) ## address, value, decimals, function code 16
        t = self.read_register(int('0x20B',16),0,3,False)   # address, decimals, func 3, unsigned
        print('Turn Direction set to {0} '.format(t))

    def write_and_set_to_cont_talker(self, rate_ms=10):
        self.write_register(int('0x238',16), rate_ms, 0, 16, False)
        print("MR320 set to continuous talker at 57600 Baud. Wait 250ms before transmitting position.")

    def write_and_set_to_modbus(self):
        self.write_register(int('0x238',16), 0, 0, 16, False)
        print("MR320 to continuous talker at 57600 Baud. Will wait 250ms before transmitting the first two bytes of position information.\n")

    def write_byte(self):
        self.write_register(int('0x00',16), 0, 0, 16, False)

class serialMR320( serial.Serial ):
    """Instrument class for MR320 fiber optic rotary encoder controller.

    Args:
        * portname (str): port name
        baudrate (int): integer
    """

    SLAVE_ADDRESS = 234 # default; this is the MR320 controller device address

    def __init__(self, portname, baudrate=57600):
        #try:
        if True:
            serial.Serial.__init__(self,
                port=portname,
                baudrate=baudrate,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                xonxoff=False,
                rtscts=False,
                dsrdtr=False,
                timeout=None,
                write_timeout=None)
            print('--- MR320 initialized at 57600 baudrate--------------------')
        #except:
        #    print('Port {0} connection failed'.format(portname))
        #    sys.exit(0)
    def get_position(self):
        ## Read Position Information 2 bytes
        b2 = self.read(2)
        if b2 is not '':
            pos = struct.unpack("h", b2)
            return pos
        else:
            return None
    def stop_talker(self):
        """ Stop continous output mode of MR320 """
        stop_command = bytearray([0x04, 0x30, 0x30, 0x02, 0x31, 0x42, 0x30, 0x03, 0x40])
        self.write(stop_command)
        self.flush()
        self.close()
        print("Serial MR320: stopped continous talker output.")

    def start_talker(self):
        """ Starts MR320 back into talker mode """
        start_command = bytearray([0x04, 0x30, 0x30, 0x02, 0x31, 0x42, 0x32, 0x03, 0x42])
        self.write(start_command)
        self.flush()


def save_data(trial_ball_racket_pos_data, save_path='test.json'):
    """Saves data results from simulation into specified save_path."""
    # Saving the objects:
    with open(save_path, 'wb') as f:  # Python 1: open(..., 'w')
        data = {'time' : trial_ball_racket_pos_data[0],
                'trial_ball_racket_pos_data' : trial_ball_racket_pos_data[1]}
        json.dump(data, f)

def main():


    from psychopy import core
    import numpy as np
    import time
    # pos_list = []
    comport = '/dev/ttyUSB0'
    #comport = 'COM6'

    mr320 = MR320(comport)
    mr320.print_config()
    mr320.write_and_set_to_cont_talker(1)
    mr320.serial.close()
    core.wait(0.250)
    # #####################################
    mr320 = serialMR320(comport)
    #mr3201.start_talker()
    print("started serial")
    pos = []
    tt = []

    import timeit
    t1 = timeit.default_timer()

    time_fcall = []
    while True:

        #core.wait(0.0005)
        #print(mr320.get_position())

        startTime = timeit.default_timer()
        pos.append(mr320.get_position())
        #mr320.get_position()
        endTime = timeit.default_timer()
        ttt = endTime - startTime
        tt.append(ttt)
        t = timeit.default_timer()-t1
        #t = time.clock()-t1
        #tt.append(t)
        if ttt < 0.0009:
            print ttt*1000
        if ttt > 0.0011:
            print ttt*1000
        if t > 10.0:
            break
    mr320.stop_talker()

    import matplotlib.pyplot as plt
    import seaborn as sns
    print len(tt)
    tt_diff = tt
    # k = 0
    # tt_diff = np.diff(tt)
    # print('greater', np.sum(tt_diff > 0.0011))
    # print('fewer', np.sum(tt_diff < 0.0009))
    plt.hist(tt_diff, bins=100, normed=True)
    plt.show()
    #save_data([time, pos])



    # print("stopped talker, wait 1 sec\n")
    # clock = core.Clock()
    # t1 = clock.getTime()
    # while True:
    #     #mr320.get_position()
    #     if clock.getTime()-t1 > 1.0:
    #         break
    #
    # mr320 = serialMR320(comport)
    # mr320.start_talker()
    #
    #
    # #mr320.start_talker()
    # print("Started talker again at 2ms sampling\n")
    # clock = core.Clock()
    # t1 = clock.getTime()
    # while True:
    #     try:
    #         print(mr320.get_position())
    #     except:
    #         print("cou;tn")
    #     if clock.getTime()-t1 > 2.0:
    #         break



    # try:
    #     mr320 = MR320(comport)
    #     mr320.print_config()
    #     mr320.write_and_set_to_cont_talker(1)
    #     mr320.serial.close
    # except:
    #     print("couldn't")
    #
    # ser = serial.Serial(
    #     port=comport,
    #     baudrate=57600,
    #     parity=serial.PARITY_NONE,
    #     stopbits=serial.STOPBITS_ONE,
    #     bytesize=serial.EIGHTBITS,
    #     xonxoff=False,
    #     rtscts=False,
    #     dsrdtr=False,
    #     timeout=0.1,
    #     write_timeout=0.5)
    #
    # from psychopy import core
    # clock = core.Clock()
    # try:
    #     t1 = clock.getTime()
    #     while True:
    #         p = ser.read(2)
    #         if p is not '':
    #             pos = struct.unpack("h", p)
    #             #print(pos, time.clock()-t1)
    #             pos_list.append(pos)
    #             if clock.getTime()-t1 > 2.0:
    #                 break
    # except KeyboardInterrupt:
    #
    #     print("\nTried to halt.\n")
    #
    #     #mr320 = MR320(comport, 57600)
    #     #mr320.write_and_set_to_modbus()
    #     #mr320.serial.close
    #     # stop continuous talker: send a byte to mr320
    #     #serial_mr320.write(1)
    #
    #     #ser.flush()
    #     #ser.close()
    #     #ser.open()
    #     #ser.isOpen()
    #     #stop_command = "\x04\x30\x30\x02\x31\x42\x30\x03\x40"
    #     #ser.write(stop_command)
    #     stop_command = bytearray([0x04, 0x30, 0x30, 0x02, 0x31, 0x42, 0x30, 0x03, 0x40])
    #     ser.write(stop_command)
    #     ser.flush()
    #     ser.close()
    #     #mr320 = MR320(comport, 57600)
    #     #mr320.write_and_set_to_modbus()
    #     #mr320.write_byte()
    #     #mr320.serial.close
    # stop_command = bytearray([0x04, 0x30, 0x30, 0x02, 0x31, 0x42, 0x30, 0x03, 0x40])
    # ser.write(stop_command)
    # ser.flush()
    # ser.close()





if __name__ == "__main__":
    main()
