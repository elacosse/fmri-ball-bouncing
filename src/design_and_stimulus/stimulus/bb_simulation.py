
sim_dict = dict(g = pixel_per_meter*9.8/4,
                alpha = 1,
                ball_vx = 1,
                ball_state = 'roll',
                trial_running = True,
                target_hit = False)
class BB:
    __init__(self, clock=None):
        self.clock = clock

        # simulation parameters
        self.pixels_per_meter = 3368.4210526315787
        self.g = self.pixels_per_meter*9.8/4
        self.alpha = 1
        self.ball_vx = 1
        self.ball_state = 'roll'
        self.trial_running = True
        self.target_hit = False
        self.fall_height
        # racket and ball
        self.racket_y
        self.racket_height
        self.ball_x = 0
        self.ball_y =
        # simulation variables recorded
        self.ball_txy = []
        self.racket_txy = []
        self.impact_times = []
    def run_simulation(self):

        if ball_state == 'fall':
            ball_d = -(0.5)*self.g*(t-time_fall)**2
            ball_vel = -self.g*(t-time_fall)
            self.ball_y = self.fall_height + ball_d

            if (self.ball_y-self.racket_y) <= self.racket_height:
                time_impact = t
                racket_impact_y = racket_y+racket_height
                racket_impact_vel = (self.racket_txy[-1][2]-self.racket_txy[-2][2])/ \
                    (racket_txy[-1][0]-racket_txy[-2][0])
                release_vel = -sim_dict['alpha']*(ball_vel-racket_impact_vel) \
                                + racket_impact_vel
                ball_state = 'bounce'
                self.impact_times.append(impact)

        elif ball_state == 'bounce':
            ball_d = -(0.5)*sim_dict['g']*(t-time_impact)**2
            self.ball_y = racket_impact_y + (release_vel*(t-time_impact) + ball_d)
            # ball reaches apex
            if (ball_y-ball_txy[-1][2]) < 0:
                self.ball_state = 'fall'
                self.fall_height = self.ball_y
                time_fall = t

        elif ball_state == 'roll':
            self.ball_x = self.ball_x+self.ball_vx*t;
            if ball_x >= 0:
                ball_state = 'fall'
                fall_height = bar_height
                ball_x = 0
                time_fall = t
        ############################################################################
        self.ball_txy.append((t, self.ball_x, self.ball_y))
        self.racket_txy.append((t, self.racket_x, self.racket_y))
