import numpy as np
from pykalman import KalmanFilter

def kalman(dt=0.001):
    """creates kalman filter"""
    # parameters ---------------------------------------------------------------
    #dt = 0.001
    q = 10.0 ** 6  # this parameter determines how much noise is put on the
    # jerk, this is the main tuning parameter

    random_state = np.random.RandomState(0)

    # state transition model ---------------------------------------------------
    transition_matrix = [[1, dt, dt ** 2 / 2],
                         [0, 1, dt],
                         [0, 0, 1]]
    transition_offset = [0., 0., 0.]
    transition_covariance = np.array([[dt ** 5 / 20, dt ** 4 / 8, dt ** 3 / 6],
                                      [dt ** 4 / 8, dt ** 3 / 3, dt ** 2 / 2],
                                      [dt ** 3 / 6, dt ** 2 / 2, dt]]) * q

    # observation model --------------------------------------------------------
    observation_matrix = [[1, 0, 0]]
    observation_offset = [0]
    observation_covariance = [[1]]

    initial_state_mean = [0, 0, 0]
    initial_state_covariance = np.identity(3) * 1000000

    # create the filter --------------------------------------------------------
    kf = KalmanFilter(
        transition_matrix, observation_matrix, transition_covariance,
        observation_covariance, transition_offset, observation_offset,
        initial_state_mean, initial_state_covariance,
        random_state=random_state
    )

    return kf


if __name__ == '__main__':
    #multiprocessing.freeze_support() # windows thing
    kalman()
