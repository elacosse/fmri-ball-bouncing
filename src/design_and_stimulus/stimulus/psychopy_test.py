#!/usr/bin/env python
from psychopy import visual, core, event, data, logging
#from psychopy_helper import launch_window, Params
import BallBouncing
#import MR320

import multiprocessing
import numpy as np
import pandas as pd
import pickle
import os, sys
import argparse
import time




def create_top_bar(window, bar_height, width=1024/2, height=7.5,
                    ball_offset=15, fill_color='yellow', top_bar_side='left'):
    """Returns static figures in ball bouncing images."""
    # top bar figure
    if top_bar_side == 'left':
        pos = (-width/2, bar_height)
        top_bar = visual.Rect(window, width=width-ball_offset, height=height,
                              pos=pos, size=1, units=None, fillColor=fill_color,
                              fillColorSpace='rgb', interpolate=True)
    elif top_bar_side == 'right':
        pos = (width/2, bar_height)
        top_bar = visual.Rect(window, width=width, height=height, pos=pos,
                              size=1, units=None, fillColor=fillColor,
                              fillColorSpace='rgb', interpolate=True)
    return top_bar

def create_ball(window, pos, ball_radius=5, fill_color='white'):
    ball = visual.Circle(window, radius=ball_radius, edges=32, pos=pos, size=1,
                         units=None, fillColor=fill_color, fillColorSpace='rgb',
                         interpolate=True)
    return ball

def create_racket(window, pos, width=1024/6, height=7.5, fill_color='yellow'):
    racket = visual.Rect(window, width=width, height=height, pos=pos, size=1,
                         units=None, fillColor=fill_color, fillColorSpace='rgb',
                         interpolate=True)
    return racket


def main(arglist):
    """Run main experiment"""

    SCREENSIZE = [1024,786]
    TRIGGER_KEY = ['w', 'W']
    window = visual.Window(SCREENSIZE, monitor="testMonitor", units="pix",
                            screen=1, allowGUI=False, color=[0,0,0],
                            colorSpace='rgb', fullscr=False, waitBlanking=False,
                            blendMode='avg', checkTiming=True)
    window.setRecordFrameIntervals(True)
    #window._refreshThreshold=1/60.0 + 0.005
    logging.console.setLevel(logging.WARNING)
    ################# Initialize Environment ###################################
    # Create shared variables for simulation
    wait_time = 1.0
    height_res = 1280.0
    ball_radius = 5
    bar_height = height_res/2/3
    #lock = multiprocessing.Lock()
    sim_t = multiprocessing.Value('d', 0.0)
    ball_x = multiprocessing.Value('d', -100)
    ball_y = multiprocessing.Value('d', bar_height+ball_radius*2)
    racket_y = multiprocessing.Value('d', -height_res/4)
    run_sim = multiprocessing.Value('b', True)

    bb_sim = BallBouncing.BallBouncing('ball bouncing simulation', sim_t,
                        fall_height=bar_height+ball_radius*2, racket_y=racket_y,
                        ball_x=ball_x, ball_y=ball_y, run_sim=run_sim,
                        wait_time=wait_time, save_path=save_path,
                        multiprocess=True)

    inc_counter = multiprocessing.Value('d', 0)
    init_racket_pos = racket_y.value

    # Generate Stimulus figures
    top_bar = create_top_bar(window, bar_height)
    racket = create_racket(window, (bb_sim.racket_x, bb_sim.racket_y.value),
        height=bb_sim.racket_height)
    ball = create_ball(window, (bb_sim.ball_x.value, bb_sim.ball_y.value), ball_radius)

    time = []
    pp_bbsim_delta_t = []
    racket_y_list = []
    ball_y_list = []
    ############################################################################
    TR = 4
    while TR <= 4: # display blank screen at start of scan run for 4 TRs
        if event.getKeys(TRIGGER_KEY):
            window.flip()
            TR += 1
    #rwm.start() # start recording from mr320
    bb_sim.start() # start ball boucning simulation
    run_clock = core.Clock() # psychopy clock
    while (TR <= 10):
        t = run_clock.getTime()

        ########################################################################
        sim_time = bb_sim.t.value
        ball.pos = (bb_sim.ball_x.value, bb_sim.ball_y.value)
        racket.pos = (bb_sim.racket_x, bb_sim.racket_y.value)
        #timeit2 = run_clock.getTime() - t
        ########################################################################
        top_bar.draw()
        racket.draw()
        if bb_sim.ball_y.value > bb_sim.racket_y.value: # don't display ball if below racket.
            ball.draw()
        window.flip()
        # #timeit1 = run_clock.getTime() - t
        ########################################################################
        if event.getKeys(TRIGGER_KEY):
            TR += 1

        pp_bbsim_delta_t.append(t-sim_time)
        time.append(sim_time)

    run_sim.value = False
    core.wait(3.0) # wait to close mr320 before terminating! Also additional time saving
    bb_sim.terminate()


    plot_frames = False
    if plot_frames:
        import pylab
        pylab.plot(window.frameIntervals)
        pylab.show()
        df1 = pd.DataFrame(pp_bbsim_delta_t)
        pylab.plot(pp_bbsim_delta_t)
        pylab.show()

def run_trial(num_trs ):
    bb_sim.start() # start ball boucning simulation
    run_clock = core.Clock() # psychopy clock
    while (TR <= 10):
        t = run_clock.getTime()

        ########################################################################
        sim_time = bb_sim.t.value
        ball.pos = (bb_sim.ball_x.value, bb_sim.ball_y.value)
        racket.pos = (bb_sim.racket_x, bb_sim.racket_y.value)
        #timeit2 = run_clock.getTime() - t
        ########################################################################
        top_bar.draw()
        racket.draw()
        if bb_sim.ball_y.value > bb_sim.racket_y.value: # don't display ball if below racket.
            ball.draw()
        window.flip()
        # #timeit1 = run_clock.getTime() - t
        ########################################################################
        if event.getKeys(TRIGGER_KEY):
            TR += 1

        pp_bbsim_delta_t.append(t-sim_time)
        time.append(sim_time)

    run_sim.value = False
    core.wait(3.0) # wait to close mr320 before terminating! Also additional time saving
    bb_sim.terminate()

    df1 = pd.DataFrame(pp_bbsim_delta_t)

if __name__ == "__main__":
    main(sys.argv[1:])
"""
function [data, impact, error] = newRhythmicBounce(window, hits, g, numBounces)

    % initialize data acquisition
    nidaqmx = daq.ni.NIDAQmx;
    [status.task, status.channel, status.start, encoderHandle] = initTask(nidaqmx, 'encoder');
    [statusB.task, statusB.channel, statusB.start, brakeHandle] = initTask(nidaqmx, 'brake');
    [statusC.task, statusC.channel, statusC.start, timestamperHandle] = initTask(nidaqmx, 'timestamper');
    [s, lh, fid] = initAcq();

    brake_length = .015;


    % initialize graphics
    alpha = .8;
    ballvx = .5;
    ballState = 'roll';
    trialRunning = true;
    targetHit = false;

    i=0;
    k=0;
    bounces=0;
    brakeInit = false;
    downwards = true;
    brakeLatency = 0.02; % brake latency distance.

    % Start accelerometer recording
    startAcqB(s);
    trialStart = tic;
    while (bounces < numBounces)
        i=i+1;
        time(i) = toc(trialStart);

        [status.read, racket(i)] = readValue(nidaqmx, encoderHandle);

        switch(ballState)
            case 'roll'
                ball(i) = 1.0275;
                ballx = .077+ballvx*time(i);
                if (ballx >= 1.219)
                    ballx = 1.219;
                    ballState = 'fall';
                    timeFall = time(i);
                end
            case 'fall'
                ball(i) = 1.0275+.5*-g*(time(i)-timeFall)^2;

                if (ball(i)-racket(i) <= brakeLatency) && brakeInit == false && downwards == true
                  [statusB.write] = writeValue(nidaqmx, brakeHandle, uint8(1));
                  [statusC.write] = writeValue(nidaqmx, timestamperHandle, uint8(1));
                  brakeInit = true;
                  brakeTimeIndex = i;
                  downwards = false;
                end
                if brakeInit == true && ((time(i)-time(brakeTimeIndex)) > brake_length)
                    [statusB.write] = writeValue(nidaqmx, brakeHandle, uint8(0));
                    [statusC.write] = writeValue(nidaqmx, timestamperHandle, uint8(0));
                    brakeInit = false;
                end
                if (ball(i)-racket(i) <= .002) && downwards == false
                    ballState = 'bounce';
                    %brakeInit = false;
                    fromFall = true;
                    k=k+1;
                    racketVelocity(k) = getVelocity(time(i-9:i), racket(i-9:i));
                    ballVelocity(k)= -g*(time(i)-timeFall);
                    releaseVelocity(k) = -alpha*(ballVelocity(k)-racketVelocity(k))+racketVelocity(k);
                    impactIndex(k) = i;
                    brakeTime(k) = time(brakeTimeIndex);
                    falling = true;
                    downwards = true;
                end
            case 'bounce'
                ball(i) = racket(impactIndex(k))+releaseVelocity(k)*(time(i)-time(impactIndex(k)))+.5*-g*(time(i)-time(impactIndex(k)))^2;

                % latency of brake, apply before racket impact.
                if (ball(i)-racket(i) <= brakeLatency) && brakeInit == false && downwards == true && falling == true
                  [statusB.write] = writeValue(nidaqmx, brakeHandle, uint8(1));
                  [statusC.write] = writeValue(nidaqmx, timestamperHandle, uint8(1));
                  brakeInit = true;
                  brakeTimeIndex = i;
                  downwards = false;
                end
                % Return brake to original state and brake status.
                if brakeInit == true && (time(i)-time(brakeTimeIndex)) > brake_length
                    [statusB.write] = writeValue(nidaqmx, brakeHandle, uint8(0));
                    [statusC.write] = writeValue(nidaqmx, timestamperHandle, uint8(0));
                    brakeInit = false;
                end
                % Racket impact. Change status indicator to true.
                if (ball(i)-racket(i) <= .002) && downwards == false && fromFall == false
                    k=k+1;
                    ballVelocity(k) = releaseVelocity(k-1)-g*(time(i)-time(impactIndex(k-1)));
                    racketVelocity(k) = getVelocity(time(i-9:i), racket(i-9:i));
                    releaseVelocity(k) = -alpha*(ballVelocity(k)-racketVelocity(k)) + racketVelocity(k);
                    impactIndex(k) = i;
                    brakeTime(k) = time(brakeTimeIndex);
                    downwards = true;
                    falling = false;
                end
                % Ball reaches apex, Change status indicator to false.
                if (ball(i-2)-ball(i-1) < 0) && (ball(i-1)-ball(i) > 0) % ball reaches apex
                    bounces=bounces+1;
                    error(bounces) = (-releaseVelocity(k)^2/(2*-g)+racket(impactIndex(k)))-1;
                    %if (abs(error(bounces)) < .03)
                    %    hits = hits+1;
                    %end
                    fromFall = false;
                    falling = true;
                end
        end


        Screen(window,'FillRect', [255, 255, 0], [0, 0.3471, 1.219, 0.3671]*420);
        Screen(window,'FillRect', [255, 0, 0], convertRacket(racket(i)));
        Screen(window,'FillOval', [255, 255, 255], convertBall(ballx, ball(i)));
        %Screen(window,'DrawText', sprintf('Hits: %d', hits), 1.5*420, .2*420, [0, 255, 0]);
        Screen('Flip', window, 0, 0, 2, 0);
        %[statusC.write] = writeValue(nidaqmx, timestamperHandle, uint8(0));
    end

    %error = (-releaseVelocity(k)^2/(2*-g)+racket(impactIndex(k)))-1;
    impact = struct('racketVelocity',racketVelocity, ...
                    'ballVelocity', ballVelocity, ...
                    'releaseVelocity', releaseVelocity, ...
                    'position',racket(impactIndex), ...
                    'index', impactIndex, ...
                    'error', error, ...
                    'brakeTime', brakeTime);
    data = struct('time', time, 'ball', ball, 'racket', racket);

    endAcqB(s, lh, fid);
    [status.stop, status.clear] = endTask(nidaqmx, encoderHandle);
    [statusB.stop, statusB.clear] = endTask(nidaqmx, brakeHandle);
    [statusC.stop, statusC.clear] = endTask(nidaqmx, timestamperHandle);

end

function [rect] = convertBall(xm, ym)
    rect = [xm-.0175, 1.3571-ym-.0175,xm+.0175, 1.3571-ym+.0175]*420;
end
function [rect] = convertRacket(ym)
    x_1 = 1.2190 - 0.1;
    rect = [x_1, 1.3571-ym, x_1+0.2, 1.3771-ym]*420;
end
function [ slope ] = getVelocity(time, position)
    slope = sum((time-mean(time)).*(position-mean(position)))/sum((time-mean(time)).*(time-mean(time)));
end
function [statusRead, value] = readValue(nidaqmx, taskHandle)
   [statusRead, value, ~] = nidaqmx.DAQmxReadCounterScalarF64(taskHandle, ...
                                                                        double(1), ...
                                                                        double(0), ...
                                                                        uint32(0));
end
function [statusWrite] = writeValue(nidaqmx, taskHandle, value)
    [statusWrite, ~, ~] = nidaqmx.DAQmxWriteDigitalLines(taskHandle, ...
                                                                            int32(1), ...
                                                                            uint32(1), ...
                                                                            double(1), ...
                                                                            uint32(0), ...
                                                                            value, ...
                                                                            int32(1), ...
                                                                            uint32(0));
end
function [statusTask, statusChannel, statusStart, taskHandle] = initTask(nidaqmx, taskType)
    [statusTask, taskHandle] = nidaqmx.DAQmxCreateTask(' ', uint64(0));
    switch(taskType)
        case 'encoder'
            [statusChannel] = nidaqmx.DAQmxCreateCILinEncoderChan(taskHandle, ...
                                       'Dev1/ctr1', ...
                                       ' ', ...
                                       nidaqmx.DAQmx_Val_X4, ...
                                       uint32(1), ...
                                       0, ...
                                       nidaqmx.DAQmx_Val_ALowBLow, ...
                                       nidaqmx.DAQmx_Val_Meters, ...
                                       .00027, ...
                                       0, ...
                                       ' ');
        case 'brake'
            [statusChannel] = nidaqmx.DAQmxCreateDOChan(taskHandle, ...
                                      'Dev1/port2/line7', ...
                                      ' ', ...
                                      nidaqmx.DAQmx_Val_ChanForAllLines);
        case 'timestamper'
            [statusChannel] = nidaqmx.DAQmxCreateDOChan(taskHandle, ...
                                     'Dev1/port1/line7', ...
                                     ' ', ...
                                     nidaqmx.DAQmx_Val_ChanForAllLines);
    end
    [statusStart]=nidaqmx.DAQmxStartTask(taskHandle);
end
function [statusStop, statusClear] = endTask(nidaqmx, taskHandle)
    [statusStop] = nidaqmx.DAQmxStopTask(taskHandle);
    [statusClear] = nidaqmx.DAQmxClearTask(taskHandle);
end
"""
