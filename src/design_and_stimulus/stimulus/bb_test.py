import multiprocessing
import sys, os
import BallBouncing
import RecordWristMovement

def main():
    parent_conn, child_conn = multiprocessing.Pipe()
    height_res = 1280
    ball_radius = 5
    bar_height = height_res/2/3

    from psychopy import core
    clock = core.Clock()
    bb_sim = BallBouncing.BallBouncing('ball bouncing simulation', child_conn,
                        fall_height=height_res/2/3, racket_y=-height_res/4,
                        ball_x=-10, ball_y=height_res/6+ball_radius*2,
                        clock=None, multiprocess=True)
    bb_sim.clock=clock
    bb_sim.start()

    try:
        while True:
            # receive data from child process
            bb_sim_result = parent_conn.recv()
            #print("Recv", result)
            #print "recv: ", result
            # send new data to child process
            #parent_conn.send([random.uniform(0.0, 1.0)])
    except KeyboardInterrupt:
        bb_sim.join()
        parent_conn.close()
        print "joined, exiting"

if (__name__ == "__main__"):
    main()
