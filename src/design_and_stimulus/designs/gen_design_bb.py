import numpy as np
import pandas as pd
import argparse

################################################################################
tr = 0.887 # 1.692 # seconds
bb_len = 20.0
rest_len = 10.0
num_trials = 20
################################################################################

def trunc_exp_rv(low, high, scale=0.1, size=1):
    import scipy.stats as ss
    rnd_cdf = np.random.uniform(ss.expon.cdf(x=low, scale=scale),
                                ss.expon.cdf(x=high, scale=scale),
                                size=size)
    return ss.expon.ppf(q=rnd_cdf, scale=scale)


def gen_design(save_path):
    trial_dict = {'trial_gos' : [],
                  'trial_rest_gos' : [],
                  'feedback_gos' : []}

    trial_dict['trial_gos'] = np.arange(0, (bb_len+rest_len)*num_trials,
                                        (bb_len+rest_len)).tolist()
    trial_dict['trial_rest_gos'] = [x+bb_len for x in trial_dict['trial_gos']]
    trial_dict['feedback_gos'] = [x+trunc_exp_rv(0.5, 4.75, scale=2.0, size=1)[0] for x in trial_dict['trial_rest_gos']]

    df_events = pd.DataFrame(trial_dict)
    df_events.to_csv(save_path, index=False)
    # for i in range(0, num_trials):
    #     go_onset = trial_dict['trial_gos'][i]
    #     rest_onset = trial_dict['trial_rest_gos'][i]
    #     feedback_onset = trial_dict['feedback_gos'][i]
    #     onsets =
    #
    # df_events = pd.DataFrame(trial_dict)
    # df.




################################################################################
if __name__ == '__main__':

    try:
        parser = argparse.ArgumentParser(prog = "gen_design",
                description = """ Generate design for stimulus """)
        parser.add_argument("-s", "-subject", "--subject", metavar='N', nargs='+',
                                help = "subject's name",
                                type=str)
        parser.add_argument("-r", "-run", "--run", metavar='N', nargs='+',
                                help = "run number, either 1,2,3, or 4",
                                type=str)

        args = parser.parse_args()
        subject_id = args.subject
        run_num = args.run

        save_path = 'run_'+str(run_num[0])+'_subjectid_'+str(subject_id[0])+'.csv'
        gen_design(save_path)
    except Exception as e:
        print(e)
